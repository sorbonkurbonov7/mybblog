<?php
namespace application\controllers;


use application\core\Controller;
use application\lib\Db;


class MainController extends Controller
{

	public function indexAction()
	{
		$this->view->render('Главная Страница');
	}

	public function aboutAction()
	{
		$this->view->render('Обо мне');
	}

	public function contactAction()
	{
		if (!empty($_POST))
		{
			if (!$this->model->contactValidate($_POST)) 
			{
				$this->view->message('error', $this->model->error);
			}
			mail('gkhamidova11@gmail.com', 'Сообщения из блога', $_POST['name'].'|'.$_POST['email'].'|'.$_POST['text'], 'From: sorbonkurbonov7@gmail.com');

			$this->view->message('success', 'Сообшения отправлено Администратору');
		}
		$this->view->render('Контакты');
	}

	public function postAction()
	{
		$this->view->render('Пост');
	}

	
}

